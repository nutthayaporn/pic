import development from './development';
import production from './production';

export default __DEV__ ? development : production;
