export {
  sleep,
  randomBetween,
  randomString,
  isEmail,
  removeEmpty,
  removeSpecialCharacters,
  aspectRatioCalculate,
};

const sleep = (ms) => new Promise((resolve) => setTimeout(resolve, ms));

const randomBetween = (min, max) => Math.floor(Math.random() * max) + min;

const randomString = (length = 5) => {
  var result = '';
  var characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
  var charactersLength = characters.length;
  for (var i = 0; i < length; i++) {
    result += characters.charAt(Math.floor(Math.random() * charactersLength));
  }
  return result;
};

const isEmail = (email) => {
  const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return re.test(String(email).toLowerCase());
};

const removeEmpty = (obj) => {
  for (var propName in obj) {
    if (obj[propName] === null || obj[propName] === undefined) {
      delete obj[propName];
    }
  }
  return obj;
};

const removeSpecialCharacters = (str) => str.replace(/[^\w\s]/gi, '');

const aspectRatioCalculate = ({ width, height, originalWidth, originalHeight }) => {
  let newWidth = originalWidth;
  let newHeight = originalHeight;

  if (width && height) {
    newWidth = width;
    newHeight = height;
  } else if (!width || !height) {
    if (width) {
      newWidth = width;
      newHeight = originalHeight / (originalWidth / width);
    } else if (height) {
      newHeight = height;
      newWidth = originalWidth / (originalHeight / height);
    }
  }
  return {
    newWidth,
    newHeight,
  };
};
