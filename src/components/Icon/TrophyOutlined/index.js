import React from 'react';
import Svg, { Path } from 'react-native-svg';

import { aspectRatioCalculate } from '../../../utils';

export default function TrophyOutlined({ width, height, color = '#4E5B7E' }) {
  const viewBoxWidth = 16;
  const viewBoxHeight = 18;

  const { newWidth, newHeight } = aspectRatioCalculate({
    width,
    height,
    originalWidth: viewBoxWidth,
    originalHeight: viewBoxHeight,
  });

  return (
    <Svg
      width={newWidth}
      height={newHeight}
      viewBox={`0 0 ${viewBoxWidth} ${viewBoxHeight}`}
      fill="none">
      <Path
        d="M13.609 7.922a2.65 2.65 0 002.332-2.628c0-1.278-.91-2.348-2.117-2.594V1.06h1.059V0H1.118v1.059h1.059V2.7A2.652 2.652 0 00.059 5.294a2.65 2.65 0 002.333 2.628 5.847 5.847 0 003.96 4.016l-.4 2.885H3.966L2.38 18h11.243l-1.589-3.177H10.05l-.4-2.885a5.847 5.847 0 003.96-4.016zm1.274-2.628c0 .697-.451 1.29-1.076 1.503.03-.388.012-.49.017-3a1.59 1.59 0 011.059 1.497zm-13.765 0c0-.69.443-1.279 1.059-1.497.005 2.54-.013 2.615.017 3a1.59 1.59 0 01-1.076-1.503zm2.118 1.059V1.059h9.529v5.294a4.769 4.769 0 01-5.416 4.72 4.772 4.772 0 01-4.113-4.72zm8.143 9.53l.53 1.058H4.091l.53-1.059h6.757zm-2.77-3.738l.371 2.678H7.02l.372-2.678a5.88 5.88 0 001.216 0z"
        fill={color}
      />
      <Path
        d="M8 8.47a2.65 2.65 0 002.647-2.647A2.65 2.65 0 008 3.176a2.65 2.65 0 00-2.647 2.647A2.65 2.65 0 008 8.471zm0-4.235a1.59 1.59 0 011.588 1.588A1.59 1.59 0 018 7.412a1.59 1.59 0 01-1.588-1.589A1.59 1.59 0 018 4.235z"
        fill={color}
      />
    </Svg>
  );
}
