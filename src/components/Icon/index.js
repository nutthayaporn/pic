import HomeOutlined from './HomeOutlined';
import TrophyOutlined from './TrophyOutlined';
import BellOutlined from './BellOutlined';
import UserCircleOutlined from './UserCircleOutlined';

export default { HomeOutlined, TrophyOutlined, BellOutlined, UserCircleOutlined };
