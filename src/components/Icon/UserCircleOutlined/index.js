import React from 'react';
import Svg, { Path } from 'react-native-svg';

import { aspectRatioCalculate } from '../../../utils';

export default function UserCircleOutlined({ width, height, color }) {
  const viewBoxWidth = 20;
  const viewBoxHeight = 20;

  const { newWidth, newHeight } = aspectRatioCalculate({
    width,
    height,
    originalWidth: viewBoxWidth,
    originalHeight: viewBoxHeight,
  });

  return (
    <Svg
      width={newWidth}
      height={newHeight}
      viewBox={`0 0 ${viewBoxWidth} ${viewBoxHeight}`}
      fill="none">
      <Path
        d="M10 0C4.486 0 0 4.486 0 10s4.486 10 10 10 10-4.486 10-10S15.514 0 10 0zm5.582 16.17c-.554-1.644-2.084-2.837-3.915-2.837H8.333c-1.831 0-3.36 1.194-3.913 2.838A8.304 8.304 0 011.667 10c0-4.595 3.738-8.333 8.333-8.333 4.595 0 8.333 3.738 8.333 8.333a8.305 8.305 0 01-2.751 6.17z"
        fill={color || '#4E5B7E'}
      />
      <Path
        d="M10 4.167A3.333 3.333 0 006.667 7.5v.833a3.333 3.333 0 006.666 0V7.5A3.333 3.333 0 0010 4.167z"
        fill={color || '#7180AC'}
      />
    </Svg>
  );
}
