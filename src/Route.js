import React, { useContext } from 'react';
import { ThemeContext } from 'react-native-elements';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
// import { createStackNavigator } from '@react-navigation/stack';

import { Icon } from './components';

import { Home } from './pages';

const Tab = createBottomTabNavigator();
// const Stack = createStackNavigator();

export default function Route() {
  const { theme } = useContext(ThemeContext);

  return (
    <Tab.Navigator
      initialRouteName="Home"
      tabBarOptions={{
        activeTintColor: theme.colors.primary,
        showLabel: false,
      }}>
      <Tab.Screen
        name="Home"
        component={Home}
        options={{
          tabBarLabel: '',
          tabBarIcon: ({ color }) => {
            return <Icon.HomeOutlined height={24} color={color} />;
          },
        }}
      />
      <Tab.Screen
        name="Trending"
        component={Home}
        options={{
          tabBarLabel: '',
          tabBarIcon: ({ color }) => {
            return <Icon.TrophyOutlined height={24} color={color} />;
          },
        }}
      />
      <Tab.Screen
        name="Post"
        component={Home}
        options={{
          tabBarLabel: '',
          tabBarIcon: ({ color }) => {
            return <Icon.HomeOutlined width={24} height={24} color={color} />;
          },
        }}
      />
      <Tab.Screen
        name="Notification"
        component={Home}
        options={{
          tabBarLabel: '',
          tabBarIcon: ({ color }) => {
            return <Icon.BellOutlined width={24} height={24} color={color} />;
          },
        }}
      />
      <Tab.Screen
        name="Profile"
        component={Home}
        options={{
          tabBarLabel: '',
          tabBarIcon: ({ color }) => {
            return <Icon.UserCircleOutlined width={24} height={24} color={color} />;
          },
        }}
      />
    </Tab.Navigator>
  );
}
