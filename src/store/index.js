import { createStore } from 'easy-peasy';

import languageModel from './language';
import authModel from './auth';

const store = createStore({
  language: languageModel,
  auth: authModel,
});

export default store;
